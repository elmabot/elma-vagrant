#!/usr/bin/env bash
sudo apt update
sudo apt dist-upgrade
echo "cd /vagrant/Elma" >> /home/vagrant/.bashrc
sudo apt-get install -y curl
sudo curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo npm i npm -g
NODE_ENV=Development
